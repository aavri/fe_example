# AavriAssessmentApp

- This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.3.
- Using ngrx (redux) for storing the app's state
- Redux dev tools can be used to see the store updates
- Using effects to trigger data fetching
- Visual components are Angular material 
- On the application load the "get branches" action is dispatched which is triggering an effect which in turn runs service method for data fetching.
- After the data is fetched from sever it is stored in the application store
- User can filter the list of branches typing a city name in the input field provided
- Filtering is implemented with the custom "branch-filter" pipe
- Each selected branch is presented in the extended view on the right panel
- The extended view contains all the branch details (including fake map image)

To run
- Run `npm install` 
- Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

To test:
- Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

