import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { HalifaxService } from '../services/halifax.service';
import * as actions from '../actions/halifax.actions';
import { BranchResponse } from '../model/branch-response';

@Injectable()
export class AppEffects {
    @Effect()
    loadBranches$ = this.actions$.pipe(
        ofType(actions.actionTypes.GET_BRANCHES),
        mergeMap(() =>
            this.halifaxService.getBranches().pipe(
                map((response: BranchResponse) => new actions.GetBranchesSuccess(response.data[0].Brand[0].Branch)),
                catchError(() => of(new actions.GetBranchesFailure()))
            )
        )
    );

    constructor(private actions$: Actions, private halifaxService: HalifaxService) {}
}
