import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { AppEffects } from './app.effects';
import { HttpClientModule } from '@angular/common/http';

import { Branch } from 'src/app/model/branch-response';

describe('AppEffects', () => {
    let actions$: Observable<any>;
    let effects: AppEffects;
    const branches: Branch[] = [];
    const branch: Branch = {
        Identification: '1234',
    } as any;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [AppEffects, provideMockActions(() => actions$)],
        });

        effects = TestBed.get(AppEffects);

        branches.push(branch);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
});
