import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})
export class HalifaxService {
    constructor(private http: HttpClient) {}

    getBranches() {
        // console.log('!!! fetching branches')
        return this.http.get('https://api.halifax.co.uk/open-banking/v2.2/branches');
    }
}
