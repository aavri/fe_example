import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { halifaxReducer } from './reducers/halifax.reducer';
import { BranchListComponent } from './components/branch-list/branch-list.component';
import { BranchDetailsComponent } from './components/branch-details/branch-details.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './effects/app.effects';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';

import { BranchFilterPipe } from './components/branch-list/branch-filter.pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [AppComponent, BranchListComponent, BranchDetailsComponent, BranchFilterPipe],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        StoreModule.forRoot({ branchStore: halifaxReducer }),
        StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
        EffectsModule.forRoot([AppEffects]),
        BrowserAnimationsModule,
        MatListModule,
        MatButtonModule,
        MatProgressBarModule,
        MatInputModule,
        MatSidenavModule,
        MatToolbarModule,
        FormsModule,
        MatCardModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
