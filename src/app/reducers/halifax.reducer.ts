import * as actions from '../actions/halifax.actions';
import { Branch } from '../model/branch-response';

export interface State {
    branches: Branch[];
    selectedBranch: Branch;
}

export const initialState: State = {
    branches: [],
    selectedBranch: null,
};

export function halifaxReducer(state = initialState, action: { type: string; payload: any }) {
    switch (action.type) {
        case actions.actionTypes.GET_BRANCHES:
            return state;

        case actions.actionTypes.GET_BRANCHES_SUCCESS:
            return { ...state, branches: action.payload };

        case actions.actionTypes.GET_BRANCHES_FAILURE:
            return { ...state, branches: [] };

        case actions.actionTypes.BRANCH_IS_SELECTED:
            return { ...state, selectedBranch: action.payload };

        default:
            return state;
    }
}
