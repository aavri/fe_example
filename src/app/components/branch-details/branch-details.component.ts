import { Component, OnInit, OnDestroy } from '@angular/core';
import { Branch } from 'src/app/model/branch-response';
import { Store } from '@ngrx/store';
import { State } from '../../reducers/halifax.reducer';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-branch-details',
    templateUrl: './branch-details.component.html',
    styleUrls: ['./branch-details.component.css'],
})
export class BranchDetailsComponent implements OnInit, OnDestroy {
    private subscriptions: Array<Subscription> = [];

    branch: Branch;

    constructor(private store: Store<State>) {
        this.subscriptions.push(
            store.select('branchStore').subscribe((data: State) => {
                this.branch = data.selectedBranch;
                // console.log('data', this.branch);
            })
        );
    }

    ngOnInit() {}

    ngOnDestroy() {
        this.subscriptions.forEach((subscription: Subscription) => {
            subscription.unsubscribe();
        });
    }
}
