import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchDetailsComponent } from './branch-details.component';
import { MatCardModule } from '@angular/material/card';
import { StoreModule } from '@ngrx/store';
import { halifaxReducer } from 'src/app/reducers/halifax.reducer';

describe('BranchDetailsComponent', () => {
    let component: BranchDetailsComponent;
    let fixture: ComponentFixture<BranchDetailsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatCardModule, StoreModule.forRoot({ branchStore: halifaxReducer })],
            declarations: [BranchDetailsComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BranchDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
