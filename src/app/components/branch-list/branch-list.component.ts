import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import * as actions from '../../actions/halifax.actions';
import { State } from '../../reducers/halifax.reducer';
import { Branch } from 'src/app/model/branch-response';

@Component({
    selector: 'app-branch-list',
    templateUrl: './branch-list.component.html',
    styleUrls: ['./branch-list.component.css'],
})
export class BranchListComponent implements OnInit, OnDestroy {
    private subscriptions: Array<Subscription> = [];

    branches: Branch[];

    constructor(private store: Store<State>) {
        this.subscriptions.push(
            store.select('branchStore').subscribe((data: State) => {
                this.branches = data.branches;
                // console.log('data', this.branches);
            })
        );
    }

    ngOnInit() {
        // console.info('ngOnInit');
        this.store.dispatch(new actions.GetBranches());
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subscription: Subscription) => {
            subscription.unsubscribe();
        });
    }

    onClick(branch: Branch) {
        // console.log('click: ', branch.PostalAddress.AddressLine);
        this.store.dispatch(new actions.BranchIsSelected(branch));
    }
}
