import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchListComponent } from './branch-list.component';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { BranchFilterPipe } from './branch-filter.pipe';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { StoreModule, Store } from '@ngrx/store';
import { halifaxReducer, State } from 'src/app/reducers/halifax.reducer';
import * as actions from 'src/app/actions/halifax.actions';
import { Branch } from 'src/app/model/branch-response';

describe('BranchListComponent', () => {
    let component: BranchListComponent;
    let fixture: ComponentFixture<BranchListComponent>;
    let store: Store<State>;
    const branch = {} as Branch;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                MatInputModule,
                MatListModule,
                MatProgressBarModule,
                StoreModule.forRoot({ branchStore: halifaxReducer }),
            ],
            declarations: [BranchListComponent, BranchFilterPipe],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BranchListComponent);
        component = fixture.componentInstance;
        //  fixture.detectChanges();
        store = TestBed.get(Store);
        spyOn(store, 'dispatch').and.callThrough();
        component.ngOnInit();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should dispatch an action to load branches', () => {
        const action = new actions.GetBranches();
        expect(store.dispatch).toHaveBeenCalledWith(action);
    });

    it('should dispatch an action when branch is selected', () => {
        const action = new actions.BranchIsSelected(branch);
        component.onClick(branch);
        expect(store.dispatch).toHaveBeenCalledWith(action);
    });
});
