import { Pipe, PipeTransform } from '@angular/core';
import { Branch } from 'src/app/model/branch-response';

@Pipe({
    name: 'branchfilter',
})
export class BranchFilterPipe implements PipeTransform {
    transform(items: Branch[], text: string): Branch[] {
        if (!items) {
            return [];
        }
        if (!text) {
            return items;
        }

        return items.filter((branch: Branch) => {
            const townName: string = branch.PostalAddress.TownName;
            if (townName && townName.toLowerCase().includes(text.toLowerCase())) {
                return branch;
            }
        });
    }
}
