import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BranchListComponent } from './components/branch-list/branch-list.component';
import { BranchDetailsComponent } from './components/branch-details/branch-details.component';
import { BranchFilterPipe } from './components/branch-list/branch-filter.pipe';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { StoreModule } from '@ngrx/store';
import { halifaxReducer } from './reducers/halifax.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './effects/app.effects';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { environment } from '../environments/environment';

describe('AppComponent', () => {
    let fixture: ComponentFixture<AppComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                BrowserModule,
                HttpClientModule,
                AppRoutingModule,
                StoreModule.forRoot({ branchStore: halifaxReducer }),
                StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
                EffectsModule.forRoot([AppEffects]),
                BrowserAnimationsModule,
                MatListModule,
                MatButtonModule,
                MatProgressBarModule,
                MatInputModule,
                MatSidenavModule,
                MatToolbarModule,
                FormsModule,
                MatCardModule,
            ],
            declarations: [AppComponent, BranchListComponent, BranchDetailsComponent, BranchFilterPipe],
        }).compileComponents();

        fixture = TestBed.createComponent(AppComponent);
    }));

    it('should create the app', () => {
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it(`should have as title 'aavri-assessment-app'`, () => {
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('aavri-assessment-app');
    });

    it('should render title', () => {
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('mat-toolbar').textContent).toContain('Branches');
    });

    it('should render container for navigatiomn comtainer', () => {
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('mat-sidenav-container')).toBeTruthy();
    });

});
