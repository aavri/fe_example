import { Action } from '@ngrx/store';
import { Branch } from '../model/branch-response';

export const actionTypes = {
    GET_BRANCHES: '[Halifax] Get branches',
    GET_BRANCHES_SUCCESS: '[Halifax] Get branches success',
    GET_BRANCHES_FAILURE: '[Halifax] Get branches failure',
    BRANCH_IS_SELECTED: '[Halifax] Branch is selected',
};

export class GetBranches implements Action {
    readonly type = actionTypes.GET_BRANCHES;
    constructor() {}
}

export class GetBranchesSuccess implements Action {
    readonly type = actionTypes.GET_BRANCHES_SUCCESS;
    constructor(public payload: Branch[]) {}
}
export class GetBranchesFailure implements Action {
    readonly type = actionTypes.GET_BRANCHES_FAILURE;
    constructor() {}
}

export class BranchIsSelected implements Action {
    readonly type = actionTypes.BRANCH_IS_SELECTED;
    constructor(public payload: Branch) {}
}

export type All = GetBranches | GetBranchesSuccess | GetBranchesFailure | BranchIsSelected;
