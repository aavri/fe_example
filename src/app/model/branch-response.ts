export interface BranchResponse {
    data?: DataItem[];
}

interface DataItem {
    Brand: Brand[];
}

interface Brand {
    BrandName: string;
    Branch: Branch[];
}

export interface Branch {
    Identification: string;
    SequenceNumber: string;
    Name: string;
    Type: string;
    SortCode: string[];
    Photo: string;
    CustomerSegment: string[];
    ServiceAndFacility: string[];
    Accessibility: string[];
    Note: string;
    OtherCustomerSegment: Other[];
    OtherAccessibility: Other[];
    OtherServiceAndFacility: Other[];
    Availability: Availability;
    ContactInfo: ContactInfo[];
    PostalAddress: PostalAddress;
}

interface Other {
    Code: string;
    Name: string;
    Description: string;
}

interface Availability {
    StandardAvailability: StandardAvailability;
    NonStandardAvailability: NonStandardAvailability;
}

interface StandardAvailability {
    Day: Day[];
}

interface NonStandardAvailability {
    Name: string;
    StartDate: string;
    EndDate: string;
    Notes: string;
    Day: Day[];
}

interface Day {
    Name: string;
    Notes: string;
    OpeningHours: OpeningHours[];
}

interface OpeningHours {
    OpeningTime: string;
    ClosingTime: string;
}

interface ContactInfo {
    ContactType: string;
    ContactContent: string;
    ContactDescription: string;
    OtherContactType: Other;
}

interface PostalAddress {
    AddressLine: string[];
    BuildingNumber: string;
    StreetName: string;
    TownName: string;
    CountrySubDivision: string[];
    Country: string;
    PostCode: string;
    GeoLocation: GeoLocation;
}

interface GeoLocation {
    GeographicCoordinates: GeographicCoordinates;
}
interface GeographicCoordinates {
    Latitude: string;
    Longitude: string;
}
